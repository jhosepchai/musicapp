import 'package:flutter/material.dart';
import 'package:artisapp/pages/artist.dart';
import 'package:artisapp/pages/album.dart';
import 'package:artisapp/pages/home.dart';
import 'package:artisapp/pages/lyrics.dart';
import 'package:artisapp/pages/bio.dart';
import 'package:artisapp/pages/splash.dart';

Map<String, WidgetBuilder> getRoutes() {
  return <String, WidgetBuilder>{
    '/artist': (BuildContext context) => ArtistScreen(),
    '/album': (BuildContext context) => AlbumScreen(),
    '/lyrics': (BuildContext context) => LyricsScreen(),
    '/biography': (BuildContext context) => BiographyScreen(),
    '/splash': (BuildContext context) => Splash(),
    '/': (BuildContext context) => HomePage(),
  };
}
