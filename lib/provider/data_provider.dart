import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;

class _MenuProvider {
  List<dynamic> artistas = [];
  
    List<dynamic> albumes = [];

  _MenuProvider(){
    cargarDataArtistas();
  }

  Future<List<dynamic>> cargarDataArtistas() async {
    final resp = await rootBundle.loadString('data/data.json');
    Map dataMap = json.decode(resp);
    artistas = dataMap['data'];
    
    return artistas;
  } 
    Future<List<dynamic>> cargarDataAlbumes() async {
    final resp = await rootBundle.loadString('data/albums.json');
    Map dataMap = json.decode(resp);      
    albumes = dataMap['data'];
    
    return albumes;
  } 
}

final menuProvider = new _MenuProvider();