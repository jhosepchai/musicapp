import 'package:flutter/material.dart';
import 'package:artisapp/routes/routes.dart';

void main() => runApp(App());


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color(0xFF212121),
        accentColor: Color(0xFFF50057),
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/splash',
      routes: getRoutes(),
    );
  }
}


